
//- Project : XPS Group control
//- file : HWProxy.h
//- threaded reading of the HW


#ifndef __HW_PROXY_H__
#define __HW_PROXY_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>
#include <yat/network/ClientSocket.h>

namespace xpsg
{
  // ============================================================================
  // some defines enums and constants
  // ============================================================================
  //- startup and communication errors
  const size_t MAX_COM_RETRIES = 20;
  const size_t MAX_COMMAND_RETRIES = 5;
  //- maximum 8 axes per XPS
  const size_t NB_MAX_AXES = 8;

  // ============================================================================
  #define NB_VIRTUAL_AXES 6
  #define NB_PHYSICAL_AXES 6
  // ============================================================================

  typedef enum
  {
    HWP_UNKNOWN_ERROR        = 0,
    HWP_INITIALIZATION_ERROR,
    HWP_COMMUNICATION_ERROR,
    HWP_COMMAND_ERROR,
    HWP_HARDWARE_ERROR,
    HWP_SOFTWARE_ERROR,
    HWP_NO_ERROR
  } ComState;  


  //- the status strings for ComState
  const size_t HWP_STATE_MAX_SIZE = 7;
  static const std::string hw_state_str[HWP_STATE_MAX_SIZE] =
  {
    "Unknown Error",
    "Initialisation Error",
    "Communication Error",
    "Command Error",
    "Hardware Error",
    "Software Error",
    "Communication Running"
  };

  typedef enum
  {
    TRAJ_NOT_INITIALIZED = 0,
    TRAJ_INITIALIZED,
    TRAJ_INPUT_ERROR,
    TRAJ_INPUT_CHECKED,
    TRAJ_INPUT_PROCESSED,
    TRAJ_LOADING,
    TRAJ_LOADED,
    TRAJ_GOING_TO_START_POINT,
    TRAJ_AT_START_POINT,
    TRAJ_CHECKING,
    TRAJ_CHECK_OK,
    TRAJ_RUNNING_TRAJ,
    TRAJ_DONE,
    TRAJ_CHECK_ERROR
  } TrajState;

    static const std::string traj_state_str[TRAJ_CHECK_ERROR + 1] =
  {
    "Trajectory not initialized",
    "Trajectory initialized",
    "Trajectory input error",
    "Trajectory input checked",
    "Trajectory input processed",
    "Loading trajectory",
    "Trajectory loaded",
    "Trajectory : goint to start point",
    "Trajectory : at start point",
    "Trajectory check",
    "Trajectory check OK",
    "Trajectory check ERROR"
  };
  

  //- TRAJECTORY STUFF  
  #define ERR_BASE_VELOCITY -48
  #define ERR_FATAL_INIT -20
  #define ERR_IN_INITIALIZATION -21
  #define ERR_POSITIONER_NAME -18
  #define ERR_READ_FILE -61
  #define ERR_STRING_TOO_LONG -3
  #define ERR_TRAJ_EMPTY -66
  #define ERR_TRAJ_ACC_LIMIT -69
  #define ERR_TRAJ_FINAL_VELOCITY -70
  #define ERR_TRAJ_INITIALIZATION -72
  #define ERR_TRAJ_TIME -75
  #define ERR_TRAJ_VEL_LIMIT -68
  #define ERR_WRONG_FORMAT -7
  #define ERR_WRONG_OBJECT_TYPE -8
  #define ERR_WRONG_PARAMETERS_NUMBER -9


  // ========================================================
  //---------------------------------------------------------  
  //- the YAT user messages 
  const size_t EXEC_LOW_LEVEL_MSG      = yat::FIRST_USER_MSG + 1000;
  const size_t GET_POSITION_VALUES     = yat::FIRST_USER_MSG + 1001;
  const size_t GET_GROUP_STATUS        = yat::FIRST_USER_MSG + 1002;
  const size_t GROUP_SET_POSITIONS     = yat::FIRST_USER_MSG + 1003;
  const size_t GROUP_OFF               = yat::FIRST_USER_MSG + 1004;
  const size_t GROUP_ON                = yat::FIRST_USER_MSG + 1005;

  const size_t GROUP_INITIALIZE        = yat::FIRST_USER_MSG + 1010;
  const size_t GROUP_HOME_SEARCH       = yat::FIRST_USER_MSG + 1011;
  const size_t GROUP_HOME_REF          = yat::FIRST_USER_MSG + 1012;
  const size_t GROUP_STOP              = yat::FIRST_USER_MSG + 1013;
  const size_t GROUP_KILL              = yat::FIRST_USER_MSG + 1014;
  const size_t GROUP_DEFINE_POSITION   = yat::FIRST_USER_MSG + 1015;
  const size_t GROUP_REFERENCING_START = yat::FIRST_USER_MSG + 1016;
  const size_t GROUP_REFERENCING_STOP  = yat::FIRST_USER_MSG + 1018;
  //- trajectory messages
  const size_t TRAJECTORY_LOAD         = yat::FIRST_USER_MSG + 1020;
  const size_t TRAJECTORY_GOTO_ORIGIN  = yat::FIRST_USER_MSG + 1021;
  const size_t TRAJECTORY_CHECK        = yat::FIRST_USER_MSG + 1022;
  //- n'utiliser que si la gestion du state MOVING n'est pas importante
  //- utiliser la methode publique trajectory_start si la gestion du state est importante.
  const size_t TRAJECTORY_START        = yat::FIRST_USER_MSG + 1023;
  const size_t TRAJECTORY_RESET        = yat::FIRST_USER_MSG + 1024;



  //- structures for use with messages service to write values in the BT500
  //- the structure to write a string 
  typedef struct LowLevelMsg
  {
    std::string cmd;
  }LowLevelMsg;

  typedef struct
  {
    std::vector <double> positions;
  }positions_t;

  typedef struct
  {
    double position;
  }position_t;


  //- for trajectory loading msg
  typedef struct
  {
    long dim_x;
    long dim_y;
    std::vector <double> positions;
  }trajectory;

  //------------------------------------------------------------------------
  //- HWProxy Class
  //- read the HW 
  //------------------------------------------------------------------------
  class HWProxy : public yat4tango::DeviceTask
  {
    public :

  //- the configuration structure
  typedef struct Config
  {
    //- members
    std::string url;
    size_t port;
    size_t startup_timeout_ms;
    size_t read_timeout_ms;
    size_t periodic_timeout_ms;
    std::string group_name;
    std::vector <string> properties;
    size_t nb_axes;


    //- Ctor -------------
    Config ();
    //- Ctor -------------
    Config ( const Config & _src);
    //- Ctor -------------
    Config (std::string url );

    //- operator = -------
    void operator = (const Config & src);
  }Config;
   
    //- Constructeur/destructeur
    HWProxy (Tango::DeviceImpl * _host_device,
             Config & conf);

    virtual ~HWProxy ();

    //- the number of communication successfully done
    unsigned long get_com_error (void)   { return com_error; };
    //- the number of communication errors
    unsigned long get_com_success (void) { return com_success; };
    //- the number of commands successfully done
    unsigned long get_command_error (void)   { return command_error; };
    //- the number of command errors
    unsigned long get_command_success (void) { return command_ok; };
    //- the state and status of communication
    ComState get_com_state (void)        {return com_state; };
    std::string get_com_status (void)    { return hw_state_str [com_state]; };
    std::string get_last_error (void)    { return last_error; };
    std::string get_group_status (void);



    //-  axis position accessor /mutator
    double get_position (int postioner_number);
    void set_position (int postioner_number, double value); 
    //-  axis offset accessor /mutator
    double get_offset (int postioner_number);
    void set_offset (int postioner_number, double value); 
    //-  axis position accessor /mutator
    double get_velocity (int postioner_number);
    void set_velocity (int postioner_number, double value); 


    //- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
    std::string exec_low_level_command (std::string cmd);

    //- returns the XPS Group state machine value;
    Tango::DevState get_group_state (void);
    
    //- set all the positions at once - use this method and not the message because of state management
    void set_positions (std::vector <double> positions); 

    //- force to start the movement
    void start (void); 
    //- force state to MOVING (for ScanServer state management)
    void force_state_moving (void);

    //- requests a check for the current trajectory loaded in memory  
    //- call it directly instead of yat::Message that to get the bool (true if OK)
    bool trajectory_current_check (void);

    //- requests a start of the current trajectory loaded in memory  
    //- call it directly instead of yat::Message that to ensure state turns directly to MOVING
    void trajectory_start (void);


  protected:
	  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	  virtual void process_message (yat::Message& msg)throw (Tango::DevFailed);

  private :

    //- get values on the hard : the group positions
    bool read_positions (void);

    //- get values on the hard : the group status
    bool read_state (void);

    //- retreives the parameters velocity, acceleration, min and max jerk time
    bool sgamma_parameters_get (int posit_number, std::vector <double> &vals);

    //- retreive max velocity and acceleration for trajectories checking
    bool positioner_maximum_velocity_accel_get (void);

    //-----------------------communication stuff---------------------------
    //- get a value on the hardware
    bool write_read (std::string cmd, std::string & response);

    //- write a command (blocking) and forget it
    bool write (std::string cmd);

    //- the socket to the HXP for non blocking exchanges
    yat::ClientSocket * sock_non_blocking;
    //- the socket to the HXP for blocking commands
    yat::ClientSocket * sock_blocking;

    //- au cas ou....
    yat::Mutex lock_wr;
    yat::Mutex lock_w;

    //- trajectory stuff
    yat::Mutex traj_lock;
    //- used for internal trajectory computing and 
    struct Tr
    {
      int state;
      std::vector <double> origin_point;
      std::vector <std::string> traj;
    } trajectory_stuff;

    //- internal trajectory methods
    void trajectory_initialize (void);
    //- compute if something is wrong with traj input 
    void trajectory_check_input (trajectory *);
    void trajectory_store (trajectory *);
    void trajectory_upload (void);
    void trajectory_clear (void);
    //- requests XPS to check loaded trajectory 
    bool trajectory_check_hw (void);
    bool trajectory_check_hw_i (std::string &);

    //- the configuration
    Config conf;

    //- the host device 
    Tango::DeviceImpl * host_dev;

    //- the periodic execution time
    size_t periodic_exec_ms;
 
    //- the state and status stuff
    ComState com_state;
    std::string com_status;

    std::string last_error;

    //- for Com error attribute
    unsigned long com_error;

    //- for Com OK attribute
    unsigned long com_success;

    //- internal error counter
    unsigned long consecutive_com_errors;

    //- for command error (command refused by XPS)
    unsigned long command_ok;
    unsigned long command_error;
    unsigned long consecutive_command_error;


    //- the Group State (the Group State of the internal State Machine)
    int group_state;
    Tango::DevState tango_state;
    int request_for_moving;
    yat::Timer request_for_moving_timer;


    //- the data container
    typedef struct Element
    {
      std::string attr_name;
      std::string posit_name;
      int posit_number;
      double offset;
      double position;
      double setpoint;
      double max_velocity;
      double max_acceleration;
    //- Ctor -------------
    Element ();
    //- Ctor -------------
    Element ( const Element & _src);
    //- operator = -------
    void operator = (const Element & src);
    }Element;

    std::vector <Element> hw_data;

    //- get offset from XPSAxis AttributeProxy
    double get_offset (Tango::AttributeProxy * ap);

    std::string groupPositionCurrentGet_arg;

    //- parse property AttrList
    //- property form : AttributeName;XPSPositionerName;XPSAxisOffsetAttributeProxyName
    //- fills the hw_data map with <positionerName, element>
    //- returns false if error occured
    bool parse_properties (std::vector <std::string> lines);
    bool parse_property (std::string line);
  };

}//- namespace
#endif //- __HW_PROXY_H__
