
//- Project : XPS GRoup Control
//- file : HWProxy.cpp
//- threaded reading of the HW

#include <task/HWProxy.h>
#include <math.h>
#include "StringTokenizer.h"
#include "task/Util.h"
#include <iomanip>


namespace xpsg
{
  // ============================================================================
  // Some defines and constants
  // ============================================================================
  static const double __NAN__ = ::sqrt(-1.);

  //- non standard VC libraries for is equal to __NAN__ test (see isnan)
  #if (_MSC_VER)
    #include <cfloat>
    #define isnan _isnan
  #endif

#define MAX_ERRORS_ADMISSIBLE 20

  // ============================================================================
  // Config::Config
  // ============================================================================
  HWProxy::Config::Config ()
  {
    url                 = "Not Initialised";
    port                = 5001;
    group_name          = "NOT_INITIALIZED";
    startup_timeout_ms  = 5000;
    read_timeout_ms     = 1000;
    periodic_timeout_ms = 100;
  }

  HWProxy::Config::Config (const Config & _src)
  {
    *this = _src;
  }
  // ============================================================================
  // Config::operator =
  // ============================================================================
  void HWProxy::Config::operator = (const Config & _src)
  {
    url                 = _src.url;
    port                = _src.port;
    group_name          = _src.group_name;
    properties          = _src.properties;
    startup_timeout_ms  = _src.startup_timeout_ms;
    read_timeout_ms     = _src.read_timeout_ms;
    periodic_timeout_ms = _src.periodic_timeout_ms;
  }

  // ============================================================================
  // Element::Element
  // ============================================================================
  HWProxy::Element::Element ()
  {
    attr_name        = "Not Initialised";
    posit_name       = "Not Initialised";
    offset           = __NAN__;
    position         = __NAN__;
    setpoint         = __NAN__;
    max_velocity     = __NAN__;
    max_acceleration = __NAN__;
  }

  HWProxy::Element::Element (const Element & _src)
  {
    *this = _src;
  }
  // ============================================================================
  // Element::operator =
  // ============================================================================
  void HWProxy::Element::operator = (const Element & _src)
  {
    attr_name        = _src.attr_name;
    posit_name       = _src.posit_name;
    offset           = _src.offset;
    position         = _src.position;
    setpoint         = _src.setpoint;
    max_velocity     = _src.max_velocity;
    max_acceleration = _src.max_acceleration;
}

  //-----------------------------------------------
  //- Ctor ----------------------------------------
  HWProxy::HWProxy (Tango::DeviceImpl * _host_device, 
                    Config & _conf) : 
              yat4tango::DeviceTask(_host_device),
              conf (_conf),
              host_dev (_host_device),
              sock_non_blocking (0),
              sock_blocking (0)
  {
    DEBUG_STREAM << "HWProxy::HWProxy <- " << std::endl;

    //- yat::Task configure optional msg handling
    this->enable_timeout_msg(true);
    this->enable_periodic_msg(true);
    this->set_periodic_msg_period(this->conf.periodic_timeout_ms);

    this->command_ok = 0;
    this->command_error = 0;
    this->consecutive_command_error = 0;


    //- parse the property AttributeMotorList
    
    bool ok = parse_properties (conf.properties);
    if (!ok)
    {
      ERROR_STREAM << "HWProxy::HWProxy Error detected trying to parse AttributeMotorList configuration property, aborting Initialisation" << std::endl;
      com_state = HWP_INITIALIZATION_ERROR;
      this->last_error += "check property AttributeMotorList fix and restart device\n";
    }
    
    //- the number of axes of the group 
    conf.nb_axes = conf.properties.size ();

    //- initialize setpoints to __NAN__
    for (size_t i = 0; i < hw_data.size (); i++)
    {
      hw_data[i].position       = __NAN__;
      hw_data[i].setpoint       = __NAN__;
      hw_data[i].offset         = __NAN__;
      hw_data[i].posit_number   = -1;
    }

    //- build the groupPositionCurrentGet_arg
    groupPositionCurrentGet_arg.clear ();
    for (size_t i = 0; i < conf.properties.size (); i++)
    {
      groupPositionCurrentGet_arg += ",double*";
    }
    DEBUG_STREAM << " HWProxy::HWProxy groupPositionCurrentGet_arg = [" << groupPositionCurrentGet_arg << "]" << std::endl;


    //- create the socket
    DEBUG_STREAM << " HWProxy::HWProxy try to create socket " << std::endl;
    try
    {
      //- yat internal cooking
      yat::Socket::init();
      sock_non_blocking = new yat::ClientSocket ();

      sock_non_blocking->set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
      sock_non_blocking->set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
      sock_non_blocking->set_option(yat::Socket::SOCK_OPT_OTIMEOUT, 1000);
      sock_non_blocking->set_option(yat::Socket::SOCK_OPT_ITIMEOUT, 1000);

      sock_blocking     = new yat::ClientSocket ();
      sock_blocking->set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
      sock_blocking->set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
      sock_blocking->set_option(yat::Socket::SOCK_OPT_OTIMEOUT, 10000);
      sock_blocking->set_option(yat::Socket::SOCK_OPT_ITIMEOUT, 10000);
      //sock_blocking->set_non_blocking_mode();

      //- instanciating the Socket (default protocol is yat::Socket::TCP_PROTOCOL)
      yat::Address addr(conf.url, conf.port);

      //- connect to addr
      DEBUG_STREAM << "Connecting to peer..." << std::endl;
      sock_non_blocking->connect(addr);
      sock_blocking->connect(addr);
    }
    catch (const yat::SocketException & se)
    {
      ERROR_STREAM << "HWProxy::HWProxy trying to create socket caught yat::SocketException [" 
                   << se.errors[0].desc 
                   << "]" 
                   << std::endl;
      com_state = HWP_INITIALIZATION_ERROR;
      last_error = "Initialisation of socket on XPS failed [check cables, power on XPS, restart device]\n";
      last_error += "\ntext of error : " + se.errors[0].desc;

      for (size_t err = 0; err  < se.errors.size(); err++)
      {
        ERROR_STREAM << "Err-" << err << "::reason..." << se.errors[err].reason << std::endl;
        ERROR_STREAM << "Err-" << err << "::desc....." << se.errors[err].desc << std::endl;
        ERROR_STREAM << "Err-" << err << "::origin..." << se.errors[err].origin << std::endl;
        ERROR_STREAM << "Err-" << err << "::code....." << se.errors[err].code << std::endl;
      }
      return;
    } 
  }


  //-----------------------------------------------
  //- Dtor ----------------------------------------
  HWProxy::~HWProxy (void)
  {
    DEBUG_STREAM << "HWProxy::~HWProxy <- " << std::endl;

    DEBUG_STREAM << "Disconnecting from peer..." << std::endl;
    sock_non_blocking->disconnect();
    sock_blocking->disconnect();

    //- yat internal cooking
    yat::Socket::terminate();

    delete sock_non_blocking;
    sock_non_blocking = 0;
    delete sock_blocking;
    sock_blocking = 0;
    
  }

  //-----------------------------------------------
  //- the user core of the Task -------------------
  void HWProxy::process_message (yat::Message& _msg) throw (Tango::DevFailed)
  {
    //- The DeviceTask's lock_ -------------

    DEBUG_STREAM << "HWProxy::handle_message::receiving msg " << _msg.to_string() << std::endl;

    //- handle msg
    switch (_msg.type())
    {

      //- THREAD_INIT =======================
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message::TASK_INIT thread is starting up" << std::endl;
        //- "initialization" code goes here
        last_error = "No Error\n";
        this->request_for_moving = 0;
        trajectory_stuff.state = TRAJ_NOT_INITIALIZED;

        //- get the current positions and initialize the setpoints
        this->read_positions ();
        //- copy values in setpoint to initialize setpoint to something
        for (size_t i = 0; i < hw_data.size (); i++)
        {
          hw_data[i].setpoint = hw_data[i].position;
        }
        //- check the state
        this->group_state = -1;
        this->read_state ();
        //- get max velocity and max acceleration (store it in Element structure)
        this->positioner_maximum_velocity_accel_get ();
      } 
      break;

      //- TASK_EXIT =======================
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_EXIT thread is quitting" << std::endl;

        //- "release" code goes here
      }
      break;

      //- TASK_PERIODIC ===================
    case yat::TASK_PERIODIC:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_PERIODIC msg " << std::endl;
         //- code relative to the task's periodic job goes here
       this->read_positions ();
       this->read_state ();
     }
      break;

      //- TASK_TIMEOUT ===================
    case yat::TASK_TIMEOUT:
      {
        //- code relative to the task's tmo handling goes here
        ERROR_STREAM << "HWProxy::handle_message handling TASK_TIMEOUT msg" << std::endl;
        this->com_state = HWP_COMMUNICATION_ERROR;
        this->last_error = "HWProxy::handle_message received TASK_TIMEOUT msg\n";
      }
      break;

      //- USER_DEFINED_MSG ================
    case GET_POSITION_VALUES:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GET_POSITION_VALUES msg" << std::endl;
        this->read_positions ();
      }
      break;
    case GET_GROUP_STATUS:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GET_GROUP_STATUS msg" << std::endl;
        this->read_state ();
      }
      break;
    case GROUP_INITIALIZE:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_INITIALIZE msg" << std::endl;
        std::string cmd = "GroupInitialize(" + conf.group_name + ")";
        std::string resp;
        this->write_read (cmd, resp);
        INFO_STREAM << "HWProxy::handle_message handling GROUP_INITIALIZE response = " << resp << std::endl;
      }
      break;
    case GROUP_KILL:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_KILL msg" << std::endl;
        std::string cmd = "GroupKill(" + conf.group_name + ")";
        std::string resp;
        this->write_read (cmd, resp);
        INFO_STREAM << "HWProxy::handle_message handling GROUP_KILL response = " << resp << std::endl;
      }
      break;
    case GROUP_HOME_SEARCH:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_HOME_SEARCH msg" << std::endl;
        std::string cmd = "GroupHomeSearch(" + conf.group_name + ")";
        this->write (cmd);
      }
      break;
    case GROUP_HOME_REF:
      {
        INFO_STREAM << "HWProxy::handle_message handling GROUP_HOME_REF msg" << std::endl;
        std::string cmd = "GroupKill (" + conf.group_name + ")";
        std::string resp;
        this->write_read (cmd, resp);
        //- GroupInitialize MUST BE BLOCKING if you want GroupHomeSearch to succeed
        cmd = "GroupInitialize (" + conf.group_name + ")";
        this->write_read (cmd, resp);
        //- Finally GroupHomeSearch MUST NOT BE BLOCKING
        cmd = "GroupHomeSearch (" + conf.group_name + ")";
        this->write (cmd);
      }
      break;
    case GROUP_STOP:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_STOP msg" << std::endl;
        std::string cmd = "GroupMoveAbort(" + conf.group_name + ")";
        std::string response;
        this->write_read (cmd, response);
      }
      break;
    case GROUP_OFF:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_OFF msg" << std::endl;
        std::string cmd = "GroupMotionDisable(" + conf.group_name + ")";
        std::string response;
        this->write_read (cmd, response);
      }
      break;
    case GROUP_ON:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_ON msg" << std::endl;
        std::string cmd = "GroupMotionEnable(" + conf.group_name + ")";
        std::string response;
        this->write_read (cmd, response);
      }
      break;
    case GROUP_REFERENCING_START:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_REFERENCING_START msg" << std::endl;
        std::string cmd = "GroupReferencingStart(" + conf.group_name + ")";
        std::string response;
        this->write_read (cmd, response);
        INFO_STREAM << "HWProxy::handle_message handling GROUP_REFERENCING_START reponse = " << response << std::endl;
      }
      break;
    case GROUP_REFERENCING_STOP:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_REFERENCING_STOP msg" << std::endl;
        std::string cmd = "GroupReferencingStop(" + conf.group_name + ")";
        std::string response;
        this->write_read (cmd, response);
std::cout << "HWProxy::handle_message handling GROUP_REFERENCING_STOP reponse = " << response << std::endl;
      }
      break;
  case GROUP_DEFINE_POSITION:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling GROUP_DEFINE_POSITION msg" << std::endl;
        //- get msg data...
        positions_t * pos = 0;
        _msg.detach_data (pos);
        std::string cmd;
        std::string resp;
        if (pos) 
        {
        cmd = "GroupKill(" + conf.group_name + ")";
        this->write_read (cmd, resp);
INFO_STREAM << "HWProxy::handle_message::GROUP_DEFINE_POSITION cmd " << cmd << " returned " << resp << std::endl;
         cmd = "GroupInitialize(" + conf.group_name + ")";
        this->write_read (cmd, resp);
INFO_STREAM << "HWProxy::handle_message::GROUP_DEFINE_POSITION cmd " << cmd << " returned " << resp << std::endl;
        cmd = "GroupReferencingStart(" + conf.group_name + ")";
        this->write_read (cmd, resp);
INFO_STREAM << "HWProxy::handle_message::GROUP_DEFINE_POSITION cmd " << cmd << " returned " << resp << std::endl;
          std::stringstream s;
          for (size_t i = 0;i < hw_data.size ();i++)
          {
             s.str ("");
            s << "GroupReferencingActionExecute("
              << conf.group_name
              << "."
              << hw_data[i].posit_name
              << ",SetPosition,None,"
              << pos->positions [i]
              << ")"
              << std::endl;
            this->write_read (s.str (), resp);
INFO_STREAM << "HWProxy::handle_message handling GROUP_DEFINE_POSITION cmd " << s.str () << "  returned " << resp << std::endl;
          }
          post_msg (*this, GROUP_REFERENCING_STOP, 1000, false);
INFO_STREAM << "HWProxy::handle_message::GROUP_DEFINE_POSITION cmd " << cmd << " returned " << resp << std::endl;
          delete pos;
        }
      }
      break;
    case TRAJECTORY_LOAD:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TRAJECTORY_LOAD msg" << std::endl;
        //- get msg data...
        trajectory * tr = 0;
        _msg.detach_data (tr);
        if (tr) 
        {
          trajectory_stuff.state = TRAJ_NOT_INITIALIZED;
          //- initialize trajectory stuffs
          this->trajectory_initialize ();
          //- do some basic checks on the trajectory 
          this->trajectory_check_input (tr);
          //- store 1rst line as it is the trajectory origin in absolute position
          //- translate to relative positions and format it in ASCII strings as expected by XPS
          this->trajectory_store (tr);         
          //- send the trajectory to XPS
          this->trajectory_upload ();
          delete tr;          
        }
      }
      break;

    case TRAJECTORY_RESET:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TRAJECTORY_RESET msg" << std::endl;
        if (this->tango_state != Tango::MOVING)
        this->trajectory_clear ();
      }
      break;

    case TRAJECTORY_GOTO_ORIGIN:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TRAJECTORY_GOTO_ORIGIN msg" << std::endl;
        if (trajectory_stuff.state == TRAJ_LOADED || 
            trajectory_stuff.state == TRAJ_DONE)
        {
          //- if trajectory is processed, all ok to go to origin point
          this->force_state_moving ();
          trajectory_stuff.state = TRAJ_GOING_TO_START_POINT;
          this->set_positions (trajectory_stuff.origin_point);
        }
        else
        {
          ERROR_STREAM << "HWProxy::handle_message::TRAJECTORY_GOTO_ORIGIN Error no trajectory loaded!" << std::endl;
          //- TODO throw ???
        }
      }
      break;

    case TRAJECTORY_CHECK:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TRAJECTORY_CHECK msg" << std::endl;
        this->trajectory_check_hw ();
      }
      break;

    case TRAJECTORY_START:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TRAJECTORY_CHECK msg" << std::endl;
        this->force_state_moving ();
        this->trajectory_start ();
      }
      break;

    default:
  		  ERROR_STREAM<< "HWProxy::handle_message::unhandled msg type received" << std::endl;
  		break;
    } //- switch (_msg.type())
  } //- HWProxy::process_message



  //-----------------------------------------------
  //- start
  //- start the movement
  //-----------------------------------------------
  void HWProxy::start (void)
  {
    DEBUG_STREAM << "HWProxy::start <-" << std::endl;
    //- check there is no invalid position (marked __NAN__) (not initialized at startup?)
    std::vector <double> pos;
    for (size_t i = 0;i < hw_data.size ();i++)
    {
      if (isnan(hw_data [i].setpoint))
      {
        ERROR_STREAM << "HWProxy::start unable to set positions [some positions are invalid]" << std::endl;
        this->last_error =   "HWProxy::start unable to set positions [some positions are invalid]\n";
        Tango::Except::throw_exception("OPERATION_NOT_ALLOWED",
                                       "unable to set positions [some positions are invalid]",
                                       "HWProxy::start");
      }
      pos.push_back (hw_data [i].setpoint);
    }
    this->set_positions (pos);
  }

  //-----------------------------------------------
  //- set_positions
  //- sets the group positions  in the XPS
  //-----------------------------------------------
  void HWProxy::set_positions (std::vector <double> positions)
  {
    DEBUG_STREAM << "HWProxy::set_positions <-" << std::endl;
    //- if really moving : throw DevFailed
    if (this->group_state == 43  ||    //- HOMING
        this->group_state == 44  ||    //- MOVING
        this->group_state == 45  ||    //- TRAJECTORY
        this->group_state == 47)       //- JOGGING
    {
      ERROR_STREAM << "HWProxy::set_positions OPERATION_NOT_ALLOWED Group is already <really> moving" << std::endl;
      last_error =    "HWProxy::set_positions OPERATION_NOT_ALLOWED Group is already <really> moving";
      Tango::Except::throw_exception("OPERATION_NOT_ALLOWED", 
                                     "cannot request to move, Group is already <really> moving",
                                     "HWProxy::set_positions");
    }

    //- mark the request for the state
    this->request_for_moving = 1;
    this->request_for_moving_timer.restart ();

     //- update the internal setpoints positions_setp
    std::stringstream cmd;
    size_t i = 0;

    cmd << "GroupMoveAbsolute("
       << conf.group_name;
    //- copy in vector and in the cmd string
    for (size_t i = 0;i < hw_data.size ();i++)
    {
      hw_data [i].position = positions [i];
      cmd << "," << positions [i];
    }
    cmd << ")" 
        << std::endl;

    this->write (cmd.str ());
  }

  //-----------------------------------------------
  //- get_position
  //- returns a single axis position in the group
  //- called by dynamic attributes
  //-----------------------------------------------
  double HWProxy::get_position (int posit_number)
  {
    DEBUG_STREAM << "HWProxy::get_position <-" << std::endl;
    //- get the position value (the one that matches posit_name)
    if ((posit_number < 0) || (posit_number >= static_cast <int> (hw_data.size ())))
    {
      std::stringstream err;
      err << "HWProxy::get_position failed to get positioner name in map [check properties]" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return __NAN__;
    }
    //- get offset
    double tmp = this->get_offset (posit_number);
    //- try to get offset on XPSAxis and assume last value if an error occured
    
    if (isnan (tmp))
    {
      ERROR_STREAM << "HWProxy::get_position offset returned is __NAN__, aborting..." << std::endl;
      return __NAN__;
    }
    DEBUG_STREAM << "HWProxy::get_position request for positioner ["
                << hw_data[posit_number].posit_name
                << "> position ("
                << hw_data[posit_number].position
                << ") offset ("
                << tmp
                << ")"
                << std::endl;

    //- return value = position + offset
    return (hw_data[posit_number].position + tmp);
  }
  //-----------------------------------------------
  //- set_position
  //- sets a single position in the XPS Group
  //-----------------------------------------------
  void HWProxy::set_position (int posit_number, double value)
  {
      DEBUG_STREAM << "HWProxy::set_position <-" << std::endl;
    if ((posit_number < 0) || (posit_number >= static_cast <int> (hw_data.size ())))
    {
      std::stringstream err;
      err << "HWProxy::set_position positioner number <" << posit_number << "> out of bounds [software error ask to support]" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return;
    }

    DEBUG_STREAM << "HWProxy::set_position for positioner [" << hw_data[posit_number].posit_name << "] <-" << std::endl;

    //- get offset
    double tmp = __NAN__;
    try
    {
      tmp = this->get_offset (posit_number);
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::set_position failed to get offset from [" 
                   << hw_data[posit_number].posit_name 
                   << "] assuming last known value if valid"
                   << std::endl;
      this->last_error =  "HWProxy::set_position failed to get offset from [" 
                       + hw_data[posit_number].posit_name 
                       + "] assuming last known value if valid";
      Tango::Except::throw_exception("OPERATION_NOT_ALLOWED",
                                     "failed to get offset on device on the Attribute Proxy [caught exception]",
                                     "HWProxy::set_position");
    }

    if (isnan (tmp))
    {
      ERROR_STREAM << "HWProxy::set_position failed to get offset from [" 
                   << hw_data[posit_number].posit_name 
                   << "] and there is no valid value, set position failed"
                   << std::endl;
      this->last_error =  "HWProxy::set_position failed to get offset from [" 
                       + hw_data[posit_number].posit_name 
                       + "] and there is no valid value, set position failed";
      Tango::Except::throw_exception("OPERATION_NOT_ALLOWED",
                                     "failed to get offset on device on the Attribute Proxy",
                                     "HWProxy::set_position");
    }

    //- write value minus offset in the map
    hw_data[posit_number].setpoint = (value - tmp);
  }
  //-----------------------------------------------
  //- get offset 
  //-----------------------------------------------
  double HWProxy::get_offset (int posit_number)
  {
    DEBUG_STREAM << "HWProxy::get_offset <-" << std::endl;
    //- check argin
    if ((posit_number < 0) || (posit_number >= static_cast <int> (hw_data.size ())))
    {
      std::stringstream err;
      err << "HWProxy::get_offset failed to get positioner data in map positioner index = " << posit_number << " [check properties]" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return __NAN__;
    }
    //- return the current offset
    return hw_data[posit_number].offset;
  }

  //-----------------------------------------------
  //- set offset 
  //-----------------------------------------------
  void HWProxy::set_offset (int posit_number, double val)
  {
    DEBUG_STREAM << "HWProxy::set_offset <-" << std::endl;
    //- check argin
    if ((posit_number < 0) || (posit_number >= static_cast <int> (hw_data.size ())))
    {
      std::stringstream err;
      err << "HWProxy::set_offset failed to get positioner name in map positioner index = " << posit_number << " [check properties]" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return;
    }
    //- set the new offset
    hw_data[posit_number].offset = val;
    return;
  }

  //-----------------------------------------------
  //- get velocity 
  //-----------------------------------------------
  double HWProxy::get_velocity (int posit_number)
  {
    DEBUG_STREAM << "HWProxy::get_velocity <-" << std::endl;
    //- check argin
    if ((posit_number < 0) || (posit_number >= static_cast <int> (hw_data.size ())))
    {
      std::stringstream err;
      err << "HWProxy::get_velocity failed to get positioner data in map positioner index = " << posit_number << " [check properties]" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return __NAN__;
    }
    std::vector <double> res;
    if (this->sgamma_parameters_get (posit_number, res))
    {
      return res[0];
    }
    else
    {
      std::stringstream err;
      err << "HWProxy::get_velocity failed to get velocity" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return __NAN__;
    }
  }

  //-----------------------------------------------
  //- set velocity 
  //-----------------------------------------------
  void HWProxy::set_velocity (int posit_number, double value)
  {
    DEBUG_STREAM << "HWProxy::set_velocity <-" << std::endl;
    //- check argin
    if ((posit_number < 0) || (posit_number >= static_cast <int> (hw_data.size ())))
    {
      std::stringstream err;
      err << "HWProxy::set_velocity failed to get positioner data in map positioner index = " << posit_number << " [check properties]" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return;
    }
    std::string response;
    //- get sgamma parameters on the XPS
    std::vector <double> res;
    if (this->sgamma_parameters_get (posit_number, res))
    {
      std::stringstream s;
    s << "PositionerSGammaParametersSet("
      << conf.group_name
      << "."
      <<  hw_data[posit_number].posit_name
      << ","
      << std::fixed
      << std::setprecision (12)
      << value  << ","
      << res[1] << ","
      << res[2] << ","
      << res[3] << ")";
      this->write_read (s.str (), response);
    }
    else
    {
      std::stringstream err;
      err << "HWProxy::get_velocity failed to get velocity" << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return;
    }

  }  //-----------------------------------------------
  //- get sgamma parameters on the HW
  //-----------------------------------------------
  bool HWProxy::sgamma_parameters_get (int posit_number, std::vector <double> &vals)
  {
    DEBUG_STREAM << "HWProxy::sgamma_parameters_get <-" << std::endl;
    //- create the command string 
    std::stringstream s;
    s << "PositionerSGammaParametersGet("
      << conf.group_name
      << "."
      <<  hw_data[posit_number].posit_name
      << ",double*,double*,double*,double*)";
    std::string resp;
    if (this->write_read (s.str (), resp))
    {
      DEBUG_STREAM << "HWProxy::get_velocity cmd [" 
                   << s.str () 
                   << "] response [" 
                   << resp << "]" 
                   << std::endl;
      //- parse the response and return it
      //- response form status,velocity,acceleration,min_jerk_time,max_jerk_time,EndOfPAI
      //- response form 0,0.1789513061472,0.02960289503446,0.0603300456024,-0.02644901123962,0.0699330284329,-0.0001644046095955,EndOfAPI
      //- response form cmd_status,X,Y,Z,U,V,W,EndOfAPI
      StringTokenizer tok (",", resp);

      DEBUG_STREAM << "HWProxy::get_velocity nb of token [" << tok.get_number_of_token () << "] (conf.nb_axes + 2) [" << (conf.nb_axes + 2) << "]" << std::endl;
      if (tok.get_number_of_token () > 4)
      {
        vals.clear ();
        double tmp = __NAN__;
        for (size_t i = 1; i < tok.get_number_of_token () - 1; i++)
        {
          tmp = tok.get_token <double> (i);
          vals.push_back (tmp);
        }
        return true;
      }
      else
      {
        std::stringstream err;
        err << "HWProxy::get_sgamma_parameters_i failed to get velocity cmd sent [" 
            << s.str () 
            << "] response received[" << resp
            << "]" 
            << std::endl; 
        ERROR_STREAM << err.str ();
        this->last_error = err.str (); 
        return false;
      }
    }
    else
    {
      std::stringstream err;
      err << "HWProxy::get_sgamma_parameters_i failed to get velocity cmd sent [" 
          << s.str () 
          << "] response received[" << resp
          << "]" 
          << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return false;
    }
  }


  //-----------------------------------------------
  //- retreive max velocity and acceleration for 1 positioner for trajectories checking
  //-----------------------------------------------
  bool HWProxy::positioner_maximum_velocity_accel_get (void)
  {
    DEBUG_STREAM << "HWProxy::positioner_maximum_velocity_accel_get_i <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;
    //- create the command string 
    std::stringstream s;

    for (size_t i = 0; i < hw_data.size (); i++)
    {
      s.clear ();
      s << "PositionerMaximumVelocityAndAccelerationGet("
        << conf.group_name
        << "."
        <<  hw_data[i].posit_name
        << ",double*,double*)";
      std::string resp;
      if (this->write_read (s.str (), resp))
      {
        DEBUG_STREAM << "HWProxy::positioner_maximum_velocity_accel_get_i cmd [" 
                     << s.str () 
                     << "] response [" 
                     << resp << "]" 
                     << std::endl;
        //- parse the response and return it
        //- response form status,max velocity,max acceleration,EndOfPAI
        //- response form 0,0.6,0.6,EndOfAPI
        //- response form cmd_status,X,Y,EndOfAPI
        StringTokenizer tok (",", resp);

        DEBUG_STREAM << "HWProxy::positioner_maximum_velocity_accel_get_i nb of token [" 
                     << tok.get_number_of_token () 
                     << "]" << std::endl;
        if (tok.get_number_of_token () > 2)
        {
          //- max velocity
          hw_data [i].max_velocity = tok.get_token <double> (1);
          hw_data [i].max_acceleration = tok.get_token <double> (2);
          return true;
        }
        else
        {
          std::stringstream err;
          err << "HWProxy::positioner_maximum_velocity_accel_get failed to get velocity cmd sent [" 
              << s.str () 
              << "] response received[" << resp
              << "]" 
              << std::endl; 
          ERROR_STREAM << err.str ();
          this->last_error = err.str (); 
          return false;
        }
      }
      else
      {
        std::stringstream err;
        err << "HWProxy::positioner_maximum_velocity_accel_get failed to get velocity cmd sent [" 
            << s.str () 
            << "] response received[" << resp
            << "]" 
            << std::endl; 
        ERROR_STREAM << err.str ();
        this->last_error = err.str (); 
        return false;
      }
    } //- end for
  }


  //-----------------------------------------------
  //- read positions on the hard
  //- gets the positions on the hard
  //-----------------------------------------------
  bool HWProxy::read_positions ()
  {
    DEBUG_STREAM << "HWProxy::read_positions <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;

    std::string response;
    //- lire les positions du groupe
    //- commande : pour le groupe d' axes 
    //- GroupPositionCurrentGet (MON_GROUP,double*,double*,double*,double*)
    std::string cmd = "GroupPositionCurrentGet(" + conf.group_name + this->groupPositionCurrentGet_arg +")" ;

    if (this->write_read (cmd, response))
    {
      DEBUG_STREAM << "HWProxy::read_positions cmd [" << cmd << "] response [" << response << "]" << std::endl;
      //- parse the response and put it in the array
      //- response form 0,0.1789513061472,0.02960289503446,0.0603300456024,-0.02644901123962,0.0699330284329,-0.0001644046095955,EndOfAPI
      //- response form cmd_status,X,Y,Z,U,V,W,EndOfAPI
      StringTokenizer tok (",", response);

      DEBUG_STREAM << "HWProxy::read_positions nb of token [" << tok.get_number_of_token () << "] (conf.nb_axes + 2) [" << (conf.nb_axes + 2) << "]" << std::endl;
      if (tok.get_number_of_token () == (conf.nb_axes + 2))
      {
        for (size_t i = 0; i < hw_data.size (); i++)
        {
          hw_data[i].position = tok.get_token <double> (i+1);
        }
        return true;
      }
    }
    else
    {
      std::stringstream err;
      err << "HWProxy::read_positions failed to get positions cmd sent [" 
          << cmd 
          << "] response received[" << response 
          << "]" 
          << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return false;
    }
  }
  //-----------------------------------------------
  //- read_state
  //- get the group state
  //-----------------------------------------------
  bool HWProxy::read_state ()
  {
    DEBUG_STREAM << "HWProxy::read_state <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;

    std::string response;
     std::string cmd = "GroupStatusGet(" + conf.group_name + ",int *)";
    if (this->write_read (cmd, response))
    {
      StringTokenizer tok(",", response);
      if (tok.get_number_of_token () <= 2)
      {
        this->com_state = HWP_COMMAND_ERROR;
        this->last_error = "WProxy::read_hardware invalid command [" + cmd + "] response [" + response + "]\n";
      }
      this->group_state = tok.get_token <int> (1);
      return true;
    }
    else
    {
      std::stringstream err;
      err << "HWProxy::read_state failed to get Group Status cmd sent [" 
          << cmd 
          << "] response received [" << response 
          << "]" 
          << std::endl; 
      ERROR_STREAM << err.str ();
      this->last_error = err.str (); 
      return false;
    }
    return true;
  }
  
  //-----------------------------------------------
  //- returns the group state machine value;
  //- 
  //-----------------------------------------------
  Tango::DevState HWProxy::get_group_state (void)
  {
  
    static const double REQUEST_FOR_MOVING_TIMEOUT = 200.;
    DEBUG_STREAM << "HWProxy::get_state <-" << std::endl;
    
   //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
    {
      tango_state = Tango::FAULT;
      return tango_state;
    }
    
      

   
    //- request_for_moving management
    if (this->request_for_moving)
    {
      if (this->request_for_moving_timer.elapsed_msec () > REQUEST_FOR_MOVING_TIMEOUT)
      {
        this->request_for_moving = false;
      }  
      else
      { 
        return Tango::MOVING;
      }
    }
    //- make shure request for moving is not eternal
    //- TODO : pourquoi 2 fois???
    if (this->request_for_moving && 
        this->request_for_moving_timer.elapsed_msec () > REQUEST_FOR_MOVING_TIMEOUT)
    {
      this->request_for_moving = false;      
    }          
    
       
    if (this->group_state <   0 ||
        this->group_state <=  9 ||
        this->group_state == 41 ||
        this->group_state == 42 ||
        this->group_state == 49 ||
        this->group_state == 50 ||
        this->group_state == 63)
      return Tango::INIT;  
      
    
    //- the group_state is MOVING    
    if (this->group_state == 43  ||    //- HOMING
        this->group_state == 44  ||    //- MOVING3
        this->group_state == 45  ||    //- TRAJECTORY
        this->group_state == 47)       //- JOGGING
    {
      this->request_for_moving = 0;
      return Tango::MOVING;
    }

    //- ALARM
    if (this->group_state == 40)      //- Emergency break3
      return Tango::ALARM;  
 
    //- OFF   
    if ((this->group_state > 20 && 
        this->group_state <= 39) ||
        this->group_state == 74 ||
        this->group_state == 75 ||
        this->group_state == 76)      //- Disable (motor OFF)
      return Tango::DISABLE;  
      
    //- STANDBY
    if (this->group_state >= 10 && 
        this->group_state <= 17 &&
        this->request_for_moving == 0)
    {
      if (trajectory_stuff.state == TRAJ_GOING_TO_START_POINT)
        trajectory_stuff.state = TRAJ_AT_START_POINT;
      if (trajectory_stuff.state == TRAJ_RUNNING_TRAJ)
        trajectory_stuff.state = TRAJ_DONE;
      return Tango::STANDBY;
    }
    //- DISABLE
    if (this->group_state == 20 )
    {
      return Tango::OFF;
    }
      
    //- finally if no match found
    return Tango::UNKNOWN;   

   }
   
  //-----------------------------------------------
  //- returns the HXP status machine value;
  //- 
  //-----------------------------------------------
  std::string HWProxy::get_group_status (void)
  {
    DEBUG_STREAM << "HWProxy::get_group_status <-" << std::endl;
    
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return "Initialisation error";
      
    if (this->group_state == -1)
      return std::string ("-1  thread en cours d'initialisation");

    std::string response;     
    std::stringstream cmd;
    cmd << "GroupStatusStringGet(" << this->group_state << ", char *)";
     this->write_read (cmd.str (), response);
    cmd.str("");
    cmd << std::endl << "group state value = " << this->group_state << std::endl;
    return response + cmd.str ();    
   }

  //-----------------------------------------------
  //- write_read
  //- send command returns result
  //-----------------------------------------------
  bool HWProxy::write_read (std::string cmd, std::string & response)
  {
    DEBUG_STREAM << "HWProxy::write_read <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;

    size_t nb = 0;
    try
    {
      { //- CRITICAL SECTION
        yat::AutoMutex<> guard(this->lock_wr);
        this->sock_non_blocking->send (cmd);
        nb  = sock_non_blocking->receive(response); 
      }
      //- response empty
      if (response.size () <= 0)
      {
        this->com_state = HWP_COMMUNICATION_ERROR;
        this->last_error = "empty response" + cmd + "\n";
        return false;
      }
      //- command error
      if (response[0] != '0')
      {
        this->com_state = HWP_COMMAND_ERROR;
        StringTokenizer tok (",",response);
        std::string ret_code = tok.get_token <std::string> (0);

        { //- CRITICAL SECTION
          yat::AutoMutex<> guard(this->lock_wr);
          this->sock_non_blocking->send (std::string("ErrorStringGet(" + ret_code + ",char*)"));
          nb = sock_non_blocking->receive(response); 
        }
        ERROR_STREAM << "HWProxy::write_read command " << cmd << " refused with message [" << response << "]" << std::endl; 
        this->last_error = "command [" + cmd + "] refused with message [" + response + "]\n" ; 
        return false;
      }
      DEBUG_STREAM << "HWProxy::write_read() response = [" << response << "] length = " << nb << std::endl;
    }
    catch (const yat::SocketException & se)
    {
      ERROR_STREAM << "HWProxy::write_read()*** yat::SocketException caught ***" << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "communication failed for command " + cmd;
      for (size_t err = 0; err  < se.errors.size(); err++)
      {
        ERROR_STREAM << "Err-" << err << "::reason..." << se.errors[err].reason << std::endl;
        ERROR_STREAM << "Err-" << err << "::desc....." << se.errors[err].desc << std::endl;
        ERROR_STREAM << "Err-" << err << "::origin..." << se.errors[err].origin << std::endl;
        ERROR_STREAM << "Err-" << err << "::code....." << se.errors[err].code << std::endl;
      }
      return false;
    } 
    catch (...)
    {
      ERROR_STREAM << "HWProxy::write_read()*** Unknown exception caught ***" << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "communication failed for command " + cmd;
      return false;
    }
    this->com_state = HWP_NO_ERROR;
    return true;
  }

  //-----------------------------------------------
  //- write
  //- send a command  (on blocking socket, forget the response)
  //-----------------------------------------------
  bool HWProxy::write (std::string cmd)
  {
    DEBUG_STREAM << "HWProxy::write <-" << std::endl;
    //- case of  device proxy error
    if (com_state == HWP_INITIALIZATION_ERROR)
      return false;
    std::string response;
    
    try
    {
      { //- CRITICAL SECTION
        yat::AutoMutex<> guard(this->lock_w);
        this->sock_blocking->send (cmd);
        //- int nb = sock_blocking->receive(response); 
        INFO_STREAM << "HWProxy::write command " << cmd << " response " << response << std::endl; 
      }
    }
    catch (const yat::SocketException & se)
    {
      ERROR_STREAM << "HWProxy::write ()*** yat::SocketException caught ***" << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "communication failed for command " + cmd;
      for (size_t err = 0; err  < se.errors.size(); err++)
      {
        ERROR_STREAM << "Err-" << err << "::reason..." << se.errors[err].reason << std::endl;
        ERROR_STREAM << "Err-" << err << "::desc....." << se.errors[err].desc << std::endl;
        ERROR_STREAM << "Err-" << err << "::origin..." << se.errors[err].origin << std::endl;
        ERROR_STREAM << "Err-" << err << "::code....." << se.errors[err].code << std::endl;
      }
      return false;
    } 
    catch (...)
    {
      ERROR_STREAM << "HWProxy::write ()*** Unknown exception caught ***" << std::endl;
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "communication failed for command " + cmd;
      return false;
    }
    this->com_state = HWP_NO_ERROR;
    INFO_STREAM << "HWProxy::write returning success" << std::endl; 
    return true;
  }



  //-----------------------------------------------
  //- exec_low_level_command
  //- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
  //-----------------------------------------------
  std::string HWProxy::exec_low_level_command (std::string cmd)
  {
    DEBUG_STREAM << "HWProxy::exec_low_level_command for command [" << cmd << "\n]" << std::endl;

    return std::string ("not yet implemented");
  }

  //-----------------------------------------------
  //- parse property AttributeMotorList
  //-----------------------------------------------
  bool HWProxy::parse_properties (std::vector <std::string> lines)
  {
    DEBUG_STREAM << "HWProxy::parse_properties <-" << std::endl;
    bool ok = false;
    for (size_t i = 0; i < lines.size (); i++)
    {
      ok = parse_property (lines[i]);
      if (!ok)
        return false;
    }
    return true;
  }
  //-----------------------------------------------
  //- parse property AttributeMotorList
  //-----------------------------------------------
  bool HWProxy::parse_property (std::string line)
  {
    DEBUG_STREAM << "HWProxy::parse_property for prop [" << line << "] <-" << std::endl;
    bool ok = false;
    StringTokenizer tok (";", line);
    if (tok.get_number_of_token() < 2)
    {
      ERROR_STREAM << "HWProxy::parse_property Error detected trying to parse [" << line << "] expected [AttributeName;PositionerName;XPSAxisOffsetAttributeProxyName]" << std::endl;
      com_state = HWP_INITIALIZATION_ERROR;
      this->last_error = std::string ("Error detected trying to parse [") + line + std::string ("] expected [AttributeName;PositionerName;XPSAxisOffsetAttributeProxyName]\n");
      return false;
    }

    //- fill the structure
    Element elem;
    elem.attr_name       = tok.get_token <std::string> (0);
    elem.posit_name      = tok.get_token <std::string> (1);
    elem.offset          = __NAN__;
    elem.position        = __NAN__;
    elem.setpoint        = __NAN__;
    DEBUG_STREAM << "HWProxy::parse_property attr_name  [" << elem.attr_name << "]" << std::endl;
    DEBUG_STREAM << "HWProxy::parse_property posit_name [" << elem.posit_name << "]" << std::endl;

    //- insert into the vector
    hw_data.push_back (elem);
    
    return true;
  }



  //-----------------------------------------------
  //- force_state_moving
  //- Forces State to MOVING (for scan features)
  //-----------------------------------------------
  void HWProxy::force_state_moving (void)
  {
    DEBUG_STREAM << "HWProxy::force_state_moving () <-" << std::endl;
    //- mark the request for the state
    this->request_for_moving = true;
    this->request_for_moving_timer.restart ();
  }


  //-----------------------------------------------
  //- trajectory_initialize
  //- initializes stuff for trajectory loading
  //-----------------------------------------------
  void HWProxy::trajectory_initialize (void)
  {
    DEBUG_STREAM << "HWProxy::trajectory_initialize () <-" << std::endl;
    trajectory_stuff.state = TRAJ_NOT_INITIALIZED;
    trajectory_stuff.origin_point.clear ();
    for (size_t i = 0; i < trajectory_stuff.traj.size (); i++)
      trajectory_stuff.traj[i].clear ();
    trajectory_stuff.traj.clear ();
    trajectory_stuff.state = TRAJ_INITIALIZED;
  }

  //-----------------------------------------------
  //- trajectory_check_input
  //- checks the trajectory input (image of double)
  //-----------------------------------------------
  void HWProxy::trajectory_check_input (trajectory * tr)
  {
    if (trajectory_stuff.state != TRAJ_INITIALIZED)
    {
      ERROR_STREAM <<  "HWProxy::trajectory_check_input try to check and stuff is not initialized" << std::endl;
      Tango::Except::throw_exception("PARAMETER_ERROR",
                                     " try to check and stuff is not initialized",
                                     "HWProxy::trajectory_check_input");
    }
    DEBUG_STREAM << "HWProxy::trajectory_check_input () <-" << std::endl;
    //- size should be (((nb of positioners * 2) + 1) * nb of points)
    size_t nb_positioners = conf.nb_axes;
    std::cout << "HWProxy::trajectory_check_input nb_positioners = " << nb_positioners << std::endl;
     size_t nb_doubles = tr->positions.size ();
    std::cout << "HWProxy::trajectory_check_input nb_doubles = " << nb_doubles << std::endl;
    size_t line_size = (nb_positioners * 2) + 1;
    std::cout << "HWProxy::trajectory_check_input line_size = " << line_size << std::endl;
    if (nb_doubles % line_size != 0)
    {
      ERROR_STREAM << "HWProxy::trajectory_check_input ERROR nb_doubles % line_size = " << (nb_doubles % line_size) << std::endl;
      trajectory_stuff.state = TRAJ_INPUT_ERROR;
      Tango::Except::throw_exception("PARAMETER_ERROR",
                                     "unable to set trajectory [data amount is invalid]",
                                     "HWProxy::trajectory_check_input");
    }
      trajectory_stuff.state = TRAJ_INPUT_CHECKED;
      std::cout << "HWProxy::trajectory_check_input nb doubles is OK " << (nb_doubles % line_size) << std::endl;
    //- TODO : any other checks?
  }

  //-----------------------------------------------
  //- trajectory_store
  //- converts the trajectory input in absolute position to an array of strings in relative positions
  //-----------------------------------------------
  void HWProxy::trajectory_store (trajectory * tr)
  {
    DEBUG_STREAM << "HWProxy::trajectory_store () <-" << std::endl;

    //- input must have been checked by trajectory_check_input ()
    if (trajectory_stuff.state != TRAJ_INPUT_CHECKED)
    {
      ERROR_STREAM <<  "HWProxy::trajectory_store try to process trajectory marked as non valid [check your trajectory]" << std::endl;
      Tango::Except::throw_exception("PARAMETER_ERROR",
                                     "try to process trajectory marked as non valid [check your trajectory]",
                                     "HWProxy::trajectory_store");
    }

    
    size_t nb_lignes = tr->dim_y;
    size_t taille_ligne = tr->dim_x;
std::cout << "nb_lignes = " << nb_lignes << "  taille ligne = " << taille_ligne << std::endl;

    std::stringstream s;
    std::string resp;
    //- initializations
//    trajectory_stuff.traj.resize (nb_lignes);
    trajectory_stuff.traj.clear ();
    trajectory_stuff.origin_point.resize (conf.nb_axes);
    //- parcours ligne par ligne
    s << std::fixed
      << std::setprecision (5)
      << std::showpoint;

    for (size_t lig = 0; lig < nb_lignes; lig++)
    {
      //- cas point origine
      if (lig == 0)
      {
        for (size_t col = 1, j = 0; col < taille_ligne; col += 2, j++)
          trajectory_stuff.origin_point [j] = tr->positions [col];
      }
      else
      {
        //- preparer la string pour 1 point de passage pour le XPS
        s.str ("");
        s << "MultipleAxesPVTLoadToMemory("
          << conf.group_name
          << ","
          //- Delta Time
          << (tr->positions [(lig * taille_ligne)] - tr->positions [((lig - 1) * taille_ligne)]);
          
        for (size_t col = 1; col < taille_ligne; col += 2)
        {
           s << ","
          //- relative position
            << (tr->positions [(lig * taille_ligne) + col] - tr->positions [((lig - 1) * taille_ligne) + col]);
          s << ","
          //- velocity
            << tr->positions [(lig * taille_ligne) + col + 1];
      }
        s << ")" << std::endl;
        trajectory_stuff.traj.push_back (s.str ());
        std::cout << "Trajectory line = " << s.str () ;
      }
    }
    
    //- DEBUG : ajout a la fin d'une ligne avec tout a 0
    
    
    trajectory_stuff.state = TRAJ_INPUT_PROCESSED;
  }

  //-----------------------------------------------
  //- trajectory_upload
  //- sends the trajectory to the XPS
  //-----------------------------------------------
  void HWProxy::trajectory_upload ()
  {
    INFO_STREAM << "HWProxy::trajectory_upload () <-" << std::endl;
    trajectory_stuff.state = TRAJ_LOADING;
    std::string resp;
      
    if (trajectory_stuff.state != TRAJ_NOT_INITIALIZED && trajectory_stuff.state != TRAJ_INPUT_ERROR)
    {
      for (size_t i = 0; i < trajectory_stuff.traj.size (); i++)
      {
        this->write_read (trajectory_stuff.traj [i], resp);
        std::cout << "HWProxy::trajectory_upload () cmd " <<  trajectory_stuff.traj [i] << " returned " << resp << std::endl;
      }
    }
    trajectory_stuff.state = TRAJ_LOADED;
  }

  //-----------------------------------------------
  //- trajectory_clear
  //- clears the trajectory in memory
  //-----------------------------------------------
  void HWProxy::trajectory_clear ()
  {
    INFO_STREAM << "HWProxy::trajectory_clear () <-" << std::endl;
    std::stringstream s;
    std::string resp;
    s << "MultipleAxesPVTResetInMemory(" << conf.group_name << ")" << std::endl;
    this->write_read (s.str (),resp);
    trajectory_stuff.state = TRAJ_NOT_INITIALIZED;
    std::cout << "HWProxy::trajectory_clear () cmd " << s.str () << " returned " << resp << std::endl;  
  }

  //-----------------------------------------------
  //- trajectory_current_check
  //- public access to private trajectory checking methods 
  //-----------------------------------------------
  bool HWProxy::trajectory_current_check ()
  {
    DEBUG_STREAM << "HWProxy::trajectory_current_check () <-" << std::endl;
    trajectory_stuff.state = TRAJ_CHECKING;
    bool b = this->trajectory_check_hw ();
    return b;
  }

  //-----------------------------------------------
  //- trajectory_check_hw
  //- request XPS to check the validity of trajectory 
  //-----------------------------------------------
  bool HWProxy::trajectory_check_hw ()
  {
    DEBUG_STREAM << "HWProxy::trajectory_check_hw () <-" << std::endl;
    //-MultipleAxesPVTVerification (GroupName, "FromMemory") 
    //- request for verification
    std::stringstream cmd;
    std::string resp;
    cmd << "MultipleAxesPVTVerification("
        << conf.group_name
        << ",FromMemory)";
    this->write_read (cmd.str (),resp);
    //- check the error returned by MultipleAxesPVTVerification
    
    if (resp.find ("Error") == std::string::npos)
    {
      trajectory_stuff.state = TRAJ_CHECK_OK;
      return true;
    }    
    
    trajectory_stuff.state = TRAJ_CHECK_ERROR;
    
    size_t end_idx = resp.find_first_of (":");
    std::string err_code_str;
    err_code_str = resp.substr (8,end_idx - 8);
    std::cout << "HWProxy::trajectory_check_hw () err_code = " << err_code_str << std::endl;
    int err_code = yat::XString<int>::to_num (err_code_str);


    std::string error_txt;
    if (err_code != 0)
    {
      switch (err_code)
      {
        case ERR_BASE_VELOCITY : 
          error_txt = "ERR_BASE_VELOCITY";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_FATAL_INIT : 
          error_txt = "ERR_FATAL_INIT";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_IN_INITIALIZATION : 
          error_txt = "ERR_IN_INITIALIZATION";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_POSITIONER_NAME : 
          error_txt = "ERR_POSITIONER_NAME";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_READ_FILE : 
          error_txt = "ERR_READ_FILE";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_STRING_TOO_LONG : 
          error_txt = "ERR_STRING_TOO_LONG";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_TRAJ_EMPTY : 
          error_txt = "ERR_TRAJ_EMPTY";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_TRAJ_ACC_LIMIT : 
          error_txt = "ERR_TRAJ_ACC_LIMIT";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_TRAJ_FINAL_VELOCITY : 
          error_txt = "ERR_TRAJ_FINAL_VELOCITY";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_TRAJ_INITIALIZATION : 
          error_txt = "ERR_TRAJ_INITIALIZATION";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_TRAJ_TIME : 
          error_txt = "ERR_TRAJ_TIME";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_TRAJ_VEL_LIMIT : 
          error_txt = "ERR_TRAJ_VEL_LIMIT";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_WRONG_FORMAT : 
          error_txt = "ERR_WRONG_FORMAT";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_WRONG_OBJECT_TYPE : 
          error_txt = "ERR_WRONG_OBJECT_TYPE";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
        case ERR_WRONG_PARAMETERS_NUMBER : 
          error_txt = "ERR_WRONG_PARAMETERS_NUMBER";
          ERROR_STREAM << "trajectory_check_hw () " << error_txt << std::endl;
        break;
      }
      this->last_error = error_txt;
      Tango::Except::throw_exception("OPERATION_NOT_ALLOWED",
                                     error_txt.c_str (),
                                     "HWProxy::trajectory_check_hw");
    }
    INFO_STREAM << "HWProxy::trajectory_check_hw trajectory check success" << std::endl;
    
    //- now check positioner by positioner
    for (size_t i = 0; i < hw_data.size (); i++)
    {
      if (this->trajectory_check_hw_i (hw_data [i].posit_name) == false)
        return false;
    }
    
    std::cout << "HWProxy::trajectory_check_hw commande <"
              << cmd.str ()
              << "> retourne ["
              << resp
              << "]"
              << std::endl;
    return true;
  }

  //-----------------------------------------------
  //- trajectory_check_hw_i
  //- request XPS to check the validity of trajectory on 1 positioner
  //-----------------------------------------------
  bool HWProxy::trajectory_check_hw_i (std::string & positioner_name)
  {
    DEBUG_STREAM << "HWProxy::trajectory_check_hw_i () <-" << std::endl;
    //- MultipleAxesPVTVerificationResultGet (GroupName.PositionerName)
    //- request for each positioner
    std::string resp;
    std::stringstream cmd;
    cmd << "MultipleAxesPVTVerificationResultGet("
        << conf.group_name
        << "."
        << positioner_name
         << ",char*,double*,double*,double*,double*)";
    this->write_read (cmd.str (), resp);
    
//- TODO : Traiter en cas d'erreur les 4 infos retornées minimum position maximum position minimum velocity maximum velocity
    std::cout << "HWProxy::trajectory_check_hw_i commande <"
              << cmd.str ()
              << "> retourne ["
              << resp
              << "]"
              << std::endl;
              
    //- TODO : exception et information a l'utilisateur
    if (resp[0] != '0')
      return false;
    return true;
  }

  //-----------------------------------------------
  //- trajectory_start
  //- starts the currently loaded in memory trajectory
  //-----------------------------------------------
  void HWProxy::trajectory_start ()
  {
    DEBUG_STREAM << "HWProxy::trajectory_start () <-" << std::endl;

  this->trajectory_stuff.state = TRAJ_RUNNING_TRAJ;
    std::stringstream cmd;
   //- MultipleAxesPVTExecution (GroupName, "FromMemory", n) (n is the number of times to be sequentially executed)      }
    cmd << "MultipleAxesPVTExecution("
        << conf.group_name
        << ",FromMemory,1)";
    this->write (cmd.str ());
  }

} //- namespace
