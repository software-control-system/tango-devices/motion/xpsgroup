//=============================================================================
//
// file :        XPSGroup.h
//
// description : Include for the XPSGroup class.
//
// project :	HexapodNewport
//
// $Author: jean_coquet $
//
// $Revision: 1.4 $
// $Date: 2011-06-20 07:37:23 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/Motion/Axis/XPSGroup/src/XPSGroup.h,v $
// $Log: not supported by cvs2svn $
// Revision 1.3  2011/06/17 14:43:38  jean_coquet
// added trajectory support (not finished, not tested, experiemental)
//
// Revision 1.2  2011/05/25 07:48:23  jean_coquet
// now uses YAT4Tango Dynamic Attributes implementation
// automatic creation of 3 attributes for each motor in the group :
// * positioner positon
// * offset
// * velocity
//
// Revision 1.1  2011/02/24 17:19:51  olivierroux
// - initial import #18423
//
//
// copyleft :    Synchrotron SOLEIL 
//               L'Orme des merisiers - Saint Aubin
//		 BP48 - 91192 Gif sur Yvette
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _XPSGROUP_H
#define _XPSGROUP_H

#include <tango.h>
#include "task/HWProxy.h"
//- #include <yat4tango/DynamicAttrHelper.h>
#include <yat4tango/DynamicAttributeManager.h>
#include <yat4tango/DynamicAttribute.h>
#include <yat/threading/Mutex.h>

//using namespace Tango;

/**
 * @author	$Author: jean_coquet $
 * @version	$Revision: 1.4 $
 */

 //	Add your own constant definitions here.
 //-----------------------------------------------


namespace XPSGroup_ns
{

/**
 * Class Description:
 * controls a group of motor from Newport (XPS controllers)
 */

/*
 *	Device States Description:
 */


class XPSGroup: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attribute member data.
 */
//@{
		Tango::DevBoolean	*attr_freeze_read;
		Tango::DevBoolean	attr_freeze_write;
		Tango::DevDouble	attr_trajectory_write;
//@}

/**
 * @name Device properties
 * Device properties member data.
 */
//@{
/**
 *	IP Address of the HXP Controller
 */
	string	url;
/**
 *	port to connect to HXP
 */
	Tango::DevShort	port;
/**
 *	polling period
 */
	Tango::DevShort	period;
/**
 *	Name of the XPS Group
 */
	string	groupName;
/**
 *	each line creates a dynamic attribute
 *	syntax : AttributeName;PositionerName;[format]
 *	order  by : the order of motors in the group
 *	format is optional, default value : %7.4f
 *	
 */
	vector<string>	attributeMotorList;
//@}

/**
 *	@name Device properties
 *	Device property member data.
 */
//@{
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	XPSGroup(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	XPSGroup(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	XPSGroup(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one destructor is defined for this class */
//@{
/**
 * The object destructor.
 */	
	~XPSGroup() {delete_device();}
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method before execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name XPSGroup methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for freeze acquisition result.
 */
	virtual void read_freeze(Tango::Attribute &attr);
/**
 *	Write freeze attribute values to hardware.
 */
	virtual void write_freeze(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for trajectory acquisition result.
 */
	virtual void read_trajectory(Tango::Attribute &attr);
/**
 *	Write trajectory attribute values to hardware.
 */
	virtual void write_trajectory(Tango::WAttribute &attr);
/**
 *	Read/Write allowed for freeze attribute.
 */
	virtual bool is_freeze_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for trajectory attribute.
 */
	virtual bool is_trajectory_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for InitializeReferencePosition command.
 */
	virtual bool is_InitializeReferencePosition_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GoToPosition command.
 */
	virtual bool is_GoToPosition_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Kill command.
 */
	virtual bool is_Kill_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for DefinePosition command.
 */
	virtual bool is_DefinePosition_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for On command.
 */
	virtual bool is_On_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Off command.
 */
	virtual bool is_Off_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for TrajectoryGoToOrigin command.
 */
	virtual bool is_TrajectoryGoToOrigin_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for TrajectoryCheck command.
 */
	virtual bool is_TrajectoryCheck_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for TrajectoryReset command.
 */
	virtual bool is_TrajectoryReset_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for TrajectoryStart command.
 */
	virtual bool is_TrajectoryStart_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();
/**
 * starts the homing process
 *	@exception DevFailed
 */
	void	initialize_reference_position();
/**
 * sends the hexapod to the  6 given positions values  X Y Z U V W
 *	@param	argin	the 6 positions X Y Z U V W
 *	@exception DevFailed
 */
	void	go_to_position(const Tango::DevVarDoubleArray *);
/**
 * Stops all movements
 *	@exception DevFailed
 */
	void	stop();
/**
 * kill all movements
 *	deinitializes the HXP
 *	@exception DevFailed
 */
	void	kill();
/**
 * defines the position as the XYZUVW
 *	@param	argin	the XYZUVW positions
 *	@exception DevFailed
 */
	void	define_position(const Tango::DevVarDoubleArray *);
/**
 * enables drivers of the group
 *	@exception DevFailed
 */
	void	on();
/**
 * disables drivers of the group
 *	@exception DevFailed
 */
	void	off();
/**
 * go to trajectory start position
 *	This command is mandatory to check the trajectory
 *	@exception DevFailed
 */
	void	trajectory_go_to_origin();
/**
 * returns true if trajectory check by XPS is successfull
 *	@return	
 *	@exception DevFailed
 */
	Tango::DevBoolean	trajectory_check();
/**
 * clears the trajectory in memory
 *	@exception DevFailed
 */
	void	trajectory_reset();
/**
 * starts the trajectory 
 *	exception if not at origin point
 *	@exception DevFailed
 */
	void	trajectory_start();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	



protected :	
	//	Add your own data members here
	//-----------------------------------------
  xpsg::HWProxy * hwp;

	bool properties_missing;
	bool init_device_done;
	bool communication_error;

	std::string status_str;
  std::string last_error;

  yat::Mutex traj_write_mutex;

  typedef struct AttrData
  {
    std::string group_name;
    std::string positioner_name;
    int positioner_number;
    std::string pos_attr_name;
    std::string off_attr_name;
    std::string vel_attr_name;
    std::string display_format;
    double position_read;
    double position_write;
    //- Read and Write always equal
    double offset;
    double velocity_read;
    double velocity_write;
    //- for initialisations purpose
    bool is_starting;
    //- Ctor
    AttrData ()
    {
      group_name            = "DEFAULT_GROUP_NAME";
      positioner_name       = "DEFAULT_POSITIONER_NAME";
      positioner_number     = -1;
      pos_attr_name         = "POS_ATTR_DEFAULT_NAME";
      off_attr_name         = "OFFS_ATTR_DEFAULT_NAME";
      vel_attr_name         = "VEL_ATTR_DEFAULT_NAME";
      display_format        = "%7.4f";
      double position_read  = 0.;
      double position_write = 0.;
      double offset         = 0.;
      double velocity_read  = 0.;
      double velocity_write = 0.;
      is_starting           = true;
    }
    void operator = (const AttrData & src)
    {
      group_name            = src.group_name;
      positioner_name       = src.positioner_name;
      positioner_number     = src.positioner_number;
      pos_attr_name         = src.pos_attr_name;
      off_attr_name         = src.off_attr_name;
      vel_attr_name         = src.vel_attr_name;
      display_format        = src.display_format;
      double position_read  = src.position_read;
      double position_write = src.position_write;
      double offset         = src.offset;
      double velocity_read  = src.velocity_read;
      double velocity_write = src.velocity_write;
      is_starting           = src.is_starting;
    }
    AttrData (const AttrData & src)
    {
      *this = src;
    }
    
  } AttrData;

  //- used to store and maintain pointer validity of the AttrData (used in callbacks)
  std::vector <AttrData *> attr_data_vect;
  
  //- YAT4Tango Dynamic Attribute support
  yat4tango::DynamicAttributeManager * m_dam;

  //- contains the names of the dynamic attributes
  std::vector <std::string> attr_names; 


  //- 
  void try_to_move ();
  //- counter of attributes to write for write_attributes calls
  int nb_of_attr_to_write_count;

  //- Tango overloaded method used to manage the simultaneous writing of h,k,l
  void write_attributes_4(const Tango::AttributeValueList_4 & values, const Tango::ClntIdent &cl_ident) 
    throw (Tango::MultiDevFailed,  Tango::DevFailed, CORBA::SystemException);

  void init_dynamic_attributes (void) 
    throw (Tango::DevFailed);
  void init_dynamic_attribute (std::string & attr_name, AttrData * ad) 
    throw (Tango::DevFailed);


  //- position callback
  void position_read_callback (yat4tango::DynamicAttributeReadCallbackData& cbd);
 	void position_write_callback (yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- associated offset callback
  void offset_read_callback (yat4tango::DynamicAttributeReadCallbackData& cbd);
 	void offset_write_callback (yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- associated velocity callback
  void velocity_read_callback (yat4tango::DynamicAttributeReadCallbackData& cbd);
 	void velocity_write_callback (yat4tango::DynamicAttributeWriteCallbackData& cbd);

  //- checks for HWProxy, user data, throws DevFailed if problem
  void check_callback_valid (yat4tango::DynamicAttributeWriteCallbackData& cbd)
    throw (Tango::DevFailed);

  //- checks for HWProxy, user data, throws DevFailed if problem
  void check_callback_valid (yat4tango::DynamicAttributeReadCallbackData& cbd)
    throw (Tango::DevFailed);


//+------------------------------------------------------------------
//	internal method : save_as_prop
//	description:	internal method to save attribute value in property (template value)
//  argin : property name, value to put in
//+------------------------------------------------------------------
  template <class T> void save_as_prop (std::string prop, T val)
  {
    Tango::DbData data_put;
    Tango::DbDatum	property(prop);
    property << val;
    data_put.push_back(property);
    get_db_device()->put_property(data_put);
    return;
  }

//+------------------------------------------------------------------
//	internal method : restore_prop
//	description:	internal method to restore attribute value previously stored in property (template value)
//  argin : property name, value to put in
//+------------------------------------------------------------------
  template <class T> void restore_prop (std::string prop, T & val)
  {
    Tango::DbData	saved_prop;
	  saved_prop.push_back(Tango::DbDatum(prop));
    get_db_device()->get_property(saved_prop);
    if (saved_prop[0].is_empty() == false)	
      saved_prop[0]  >>  val;
    else
      val = 0.;
    return;
  }


};

}	// namespace_ns

#endif	// _XPSGROUP_H
